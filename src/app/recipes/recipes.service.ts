import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';

@Injectable({ providedIn: 'root' })

export class RecipesService {
    private recipes: Recipe[] = [
        {
          id: 'r1',
          title: 'Berger King',
          imageUrl: 'https://assets.myfoodandfamily.com/adaptivemedia/rendition/109542.tif?id=d95ef44db7c325918e2b7e28ac3bf3f92ed92900&ht=650&wd=1004&version=1&clid=pim',
          ingredients: ['French Fries', 'Beef Meat', 'Salad']
        },
        {
          id: 'r2',
          title: 'Spaghetti',
          imageUrl: 'https://www.starfrit.com/media/contentmanager/content/cache/1070x1070//recipes/e1_r1_spaghetti.jpg',
          ingredients: ['Cheese', 'Beef Meat', 'Tomatoes']
        }
    ];

    constructor() {}    

    getAllRecipes() {
        // return this.recipes.slice(0);
        return [...this.recipes];
    }

    getRecipe(recipeId: string) {
        return { ...this.recipes.find(recipe => {
            return recipe.id === recipeId;
        }) }
    }

    deleteRecipe(recipeId: string) {
        console.log('recipeId service', recipeId)
        this.recipes = this.recipes.filter(recipe => {
            console.log('recipe.id !== recipeId ', recipe.id !== recipeId)
            return recipe.id !== recipeId;
        });
    }
}